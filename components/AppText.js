import React from "react";
import { Text } from "react-native";
import useThemeColors from "../hooks/useThemeColors";

export default function AppText({ children, style, ...others }) {
  const colors = useThemeColors();

  return (
    <Text
      style={[
        {
          color: colors.text,
        },
        style,
      ]}
      {...others}
    >
      {children}
    </Text>
  );
}
