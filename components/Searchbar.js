import React, { useState } from "react";
import { StyleSheet, View, Animated, Dimensions } from "react-native";
import AppTextInput from "./AppTextInput";

const { width } = Dimensions.get("window");

export default function Searchbar() {
  const [query, setQuery] = useState("");

  return (
    <>
      <AppTextInput
        iconName="ios-search"
        iconSize={25}
        placeholder="Search"
        style={styles.searchRounded}
        value={query}
        onChangeText={(text) => setQuery(text)}
        fontStyle={{ fontSize: 15 }}
      />
    </>
  );
}

const styles = StyleSheet.create({
  searchRounded: {
    width: width * 0.8,
    maxWidth: 400,
    maxHeight: 40,
    elevation: 1,
    borderRadius: 5,
  },
});
