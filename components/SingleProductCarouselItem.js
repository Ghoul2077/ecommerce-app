import React from "react";
import { Dimensions, Image, Pressable, StyleSheet } from "react-native";

const { width, height } = Dimensions.get("window");

export default function ProductPageCarouselItem({ data: image }) {
  return (
    <Pressable
      style={styles.cover}
      onPress={() => console.log("Product Image Clicked")}
    >
      <Image
        style={styles.image}
        source={{ uri: image.url }}
        resizeMode="cover"
      />
    </Pressable>
  );
}

const styles = StyleSheet.create({
  cover: { height: height * 0.75, width: width },
  image: { position: "absolute", width: "100%", height: "100%" },
});
