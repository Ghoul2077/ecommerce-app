import React from "react";
import { StyleSheet, Text, View, Dimensions } from "react-native";
import useThemeColors from "../hooks/useThemeColors";
import AppButton from "./AppButton";

const { width } = Dimensions.get("window");

export default function AppToast({
  text,
  style,
  textStyle,
  buttonText,
  buttonStyle,
  onPress,
}) {
  const colors = useThemeColors();

  return (
    <View style={[styles.toast, { backgroundColor: colors.secondary }, style]}>
      <Text style={[styles.text, { marginRight: 20 }, textStyle]}>{text}</Text>
      {buttonText && (
        <AppButton
          title={buttonText}
          style={styles.button}
          textStyle={[
            styles.text,
            { textDecorationLine: "underline" },
            buttonStyle,
          ]}
          onPress={onPress}
        />
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  toast: {
    position: "absolute",
    bottom: 18,
    left: "50%",
    width: width * 0.75,
    marginLeft: -(width * 0.375),
    height: 45,
    justifyContent: "center",
    alignItems: "center",
    zIndex: 100,
    borderRadius: 25,
    elevation: 4,
    flexDirection: "row",
  },
  text: {
    color: "white",
    fontSize: 20,
    fontWeight: "bold",
  },
  button: {
    backgroundColor: "transparent",
  },
});
