import React, { useState } from "react";
import {
  StyleSheet,
  View,
  Modal,
  TouchableWithoutFeedback,
  FlatList,
} from "react-native";
import useThemeColors from "../hooks/useThemeColors";
import AppButton from "./AppButton";
import AppText from "./AppText";

export default function AppPicker({
  title,
  iconSize = 25,
  selection,
  changeSelection,
  data,
  style,
}) {
  const colors = useThemeColors();
  const [popupVisible, setPopupVisible] = useState(false);

  function PopupListItem({ item }) {
    return (
      <AppButton
        style={[
          styles.listItem,
          { backgroundColor: colors.secondary },
          item === selection && { backgroundColor: colors.secondaryDark },
        ]}
        textStyle={{ fontSize: 18 }}
        title={item}
        onPress={() => {
          setPopupVisible(false);
          changeSelection(item);
        }}
      />
    );
  }

  return (
    <>
      <AppButton
        style={[styles.button, style]}
        title={selection ? `${title}: ${selection}` : title}
        iconSize={iconSize}
        rightIconName="ios-arrow-down"
        onPress={() => setPopupVisible(true)}
      />
      <Modal visible={popupVisible} transparent>
        <TouchableWithoutFeedback onPress={() => setPopupVisible(false)}>
          <View style={styles.popupWrapper}>
            <View
              style={[styles.popupContainer, { backgroundColor: colors.white }]}
            >
              <TouchableWithoutFeedback>
                <View style={styles.popup}>
                  <AppText style={styles.prompt}>Pick a {title}</AppText>
                  <FlatList
                    scrollEnabled
                    showsVerticalScrollIndicator={false}
                    data={data}
                    keyExtractor={(item, index) => String(index)}
                    renderItem={({ item }) => <PopupListItem item={item} />}
                  />
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    </>
  );
}

const styles = StyleSheet.create({
  popupWrapper: {
    position: "absolute",
    width: "100%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.5)",
  },
  popupContainer: {
    width: 300,
    maxHeight: 450,
    alignItems: "center",
    borderRadius: 25,
    padding: 20,
  },
  popup: {
    width: "100%",
  },
  prompt: {
    fontFamily: "BebasNeue-Bold",
    fontSize: 30,
    textTransform: "capitalize",
    textAlign: "center",
    marginBottom: 10,
  },
  listItem: {
    marginVertical: 5,
    borderRadius: 25,
  },
});
