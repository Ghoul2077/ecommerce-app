import React from "react";
import { StyleSheet, View, Dimensions } from "react-native";
import useThemeColors from "../hooks/useThemeColors";
import AppButton from "./AppButton";

const { height } = Dimensions.get("window");

export default function MultiPicker({ data, toggleState, onToggle }) {
  const colors = useThemeColors();
  
  return (
    <View style={styles.container}>
      {data.map((item, index) => (
        <AppButton
          style={[
            styles.item,
            !toggleState[index] && {
              backgroundColor: colors.white,
              borderColor: colors.primary,
              elevation: 3,
            },
          ]}
          key={index}
          title={item}
          onPress={() => onToggle(index)}
          textStyle={!toggleState[index] && { color: colors.primary }}
        />
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  item: {
    width: height > 720 ? 60 : 50,
    height: height > 720 ? 60 : 50,
    borderRadius: height > 720 ? 30 : 25,
    justifyContent: "center",
    alignItems: "center",
  },
});
