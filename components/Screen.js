import React from "react";
import { SafeAreaView, StyleSheet, Platform } from "react-native";
import Constants from "expo-constants";

export default function Screen({ headerShown = false, children, style }) {
  return (
    <SafeAreaView
      style={[styles.screen, !headerShown && styles.screenPadding, style]}
    >
      {children}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
  screenPadding: {
    paddingTop: Platform.OS === "android" ? Constants.statusBarHeight : 0,
  },
});
