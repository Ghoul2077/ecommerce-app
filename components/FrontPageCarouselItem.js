import React from "react";
import { Pressable, Dimensions, StyleSheet, View, Image } from "react-native";

const { width } = Dimensions.get("window");

export default function FrontPageCarouselItem({ data }) {
  const { url: image } = data;

  return (
    <Pressable onPress={() => console.log("Product Card")}>
      <View style={styles.cover}>
        <Image
          style={styles.image}
          source={{ uri: image }}
          resizeMode="cover"
        />
      </View>
    </Pressable>
  );
}

const styles = StyleSheet.create({
  cover: {
    width: width * 0.9,
    height: 250,
    borderRadius: 10,
    marginHorizontal: width * 0.05,
    marginVertical: 1,
    elevation: 2,
    overflow: "hidden",
  },
  image: {
    width: "100%",
    height: "100%",
  },
});
