import React from "react";
import { Image, StyleSheet, View, Pressable } from "react-native";
import useThemeColors from "../hooks/useThemeColors";
import AppText from "./AppText";

export default function ProductCard({
  source,
  title,
  subtitle,
  alternateSubtitle,
  onPress,
  style,
}) {
  const colors = useThemeColors();

  return (
    <Pressable style={[styles.container, style]} onPress={onPress}>
      <View style={styles.cover}>
        <Image
          style={styles.image}
          source={{ uri: source.url }}
          resizeMode="cover"
          resizeMethod="resize"
          progressiveRenderingEnabled
        />
      </View>
      {title && (
        <View style={styles.content}>
          <AppText style={styles.title} numberOfLines={1}>
            {title}
          </AppText>
          <View style={{ flexDirection: "row" }}>
            <AppText
              style={[
                styles.subtitle,
                { color: colors.primary },
                alternateSubtitle && {
                  textDecorationLine: "line-through",
                },
              ]}
              numberOfLines={1}
            >
              {subtitle}
            </AppText>
            {alternateSubtitle && (
              <AppText
                style={[
                  styles.subtitle,
                  {
                    color: colors.error,
                  },
                ]}
                numberOfLines={1}
              >
                {alternateSubtitle}
              </AppText>
            )}
          </View>
        </View>
      )}
    </Pressable>
  );
}

const styles = StyleSheet.create({
  container: {
    width: 150,
    height: 250,
    overflow: "hidden",
    padding: 2,
    borderRadius: 10,
  },

  cover: {
    width: "100%",
    height: "80%",
    marginBottom: 10,
    overflow: "hidden",
  },

  image: {
    position: "absolute",
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },

  title: {
    fontFamily: "Montserrat-Bold",
    marginLeft: 5,
  },

  subtitle: {
    fontFamily: "Montserrat-Bold",
    marginLeft: 5,
  },
});
