import React, { useRef, useState } from "react";
import {
  ScrollView,
  StyleSheet,
  Dimensions,
  View,
  Pressable,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import AppText from "./AppText";
import ProductCard from "./ProductCard";
import useThemeColors from "../hooks/useThemeColors";

const { width } = Dimensions.get("window");

export default function ProductShowcase({
  heading,
  data,
  scrollViewStyle,
  cardStyle,
  onPress,
}) {
  const colors = useThemeColors();
  const scrollViewRef = useRef();
  const [x, setX] = useState(0);

  return (
    <>
      <View style={styles.header}>
        {heading && <AppText style={styles.heading}>{heading}</AppText>}
        <Pressable
          onPress={() => {
            scrollViewRef.current.scrollTo({
              x: x + 150,
              animated: true,
            });
            setX(x + 50);
          }}
          style={styles.arrow}
        >
          <Ionicons
            name="md-arrow-forward"
            size={24}
            style={{ color: colors.primary }}
          />
        </Pressable>
      </View>
      <ScrollView
        style={scrollViewStyle}
        horizontal
        showsHorizontalScrollIndicator={false}
        ref={scrollViewRef}
        onScrollEndDrag={(event) => setX(event.nativeEvent.contentOffset.x)}
      >
        {data.map((item, index) => (
          <ProductCard
            key={index}
            title={item.name}
            subtitle={"₹ ".concat(item.price)}
            alternateSubtitle={item.discountedPrice}
            source={item.images.items[0]}
            style={cardStyle}
            onPress={() => onPress(item.sys.id)}
          />
        ))}
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  header: {
    marginHorizontal: width * 0.05,
    marginVertical: 20,
    flexDirection: "row",
  },
  heading: {
    fontFamily: "BebasNeue-Bold",
    fontSize: 30,
  },
  arrow: {
    alignSelf: "flex-end",
    marginLeft: "auto",
  },
});
