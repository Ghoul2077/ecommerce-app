import React from "react";
import {
  Dimensions,
  Image,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
} from "react-native";

const { width } = Dimensions.get("window");

export default function Banner({ source, style, onPress }) {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={[styles.container, style]}>
        <Image
          style={styles.image}
          source={{ uri: source }}
          resizeMode="cover"
        />
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 10,
    width: width * 0.9,
    height: 150,
    marginVertical: 5,
    marginHorizontal: width * 0.05,
    overflow: "hidden",
  },
  image: { position: "absolute", width: "100%", height: "100%" },
});
