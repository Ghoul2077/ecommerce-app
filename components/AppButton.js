import React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import AppText from "./AppText";
import useThemeColors from "../hooks/useThemeColors";

export default function AppButton({
  title,
  textStyle,
  leftIconName,
  leftIconSize,
  rightIconName,
  rightIconSize,
  iconSize = 30,
  iconColor = "white",
  style,
  onPress,
}) {
  const colors = useThemeColors();

  return (
    <TouchableOpacity
      style={[styles.container, { backgroundColor: colors.primary }, style]}
      onPress={onPress}
    >
      {leftIconName && (
        <Ionicons
          name={leftIconName}
          size={leftIconSize || iconSize}
          color={iconColor}
        />
      )}
      {title && (
        <AppText
          style={[
            styles.text,
            (leftIconName || rightIconName) && styles.textWithIcon,
            textStyle,
          ]}
        >
          {title}
        </AppText>
      )}
      {rightIconName && (
        <Ionicons
          style={{ marginLeft: 10 }}
          name={rightIconName}
          size={rightIconSize || iconSize}
          color={iconColor}
        />
      )}
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 10,
    padding: 10,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
  },
  text: {
    color: "white",
    fontSize: 15,
  },
  textWithIcon: {
    textAlign: "center",
    flex: 1,
  },
});
