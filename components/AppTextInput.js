import React from "react";
import { StyleSheet, TextInput, View } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import useThemeColors from "../hooks/useThemeColors";

export default function AppTextInput({
  iconName,
  iconSize = 30,
  iconColor,
  style,
  fontStyle,
  ...others
}) {
  const colors = useThemeColors();

  return (
    <View style={[styles.container, { backgroundColor: colors.light }, style]}>
      {iconName && (
        <Ionicons
          name={iconName}
          size={iconSize}
          color={iconColor && colors.medium}
          style={styles.icon}
        />
      )}
      <TextInput
        style={[styles.input, { color: colors.text }, fontStyle]}
        {...others}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    borderRadius: 25,
    padding: 10,
    marginVertical: 10,
  },
  icon: {
    marginRight: 10,
  },
  input: {
    flex: 1,
    fontSize: 18,
  },
});
