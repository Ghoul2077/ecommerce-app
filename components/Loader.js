import React from "react";
import { StyleSheet } from "react-native";
import useThemeColors from "../hooks/useThemeColors";
import AppText from "./AppText";
import Screen from "./Screen";

export default function Loader() {
  const colors = useThemeColors();

  return (
    <Screen style={styles.loading}>
      <AppText style={[styles.loadingText, { color: colors.grey }]}>
        Loading...
      </AppText>
    </Screen>
  );
}

const styles = StyleSheet.create({
  loading: {
    justifyContent: "center",
    alignItems: "center",
  },
  loadingText: {
    fontSize: 15,
  },
});
