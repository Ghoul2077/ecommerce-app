import React, { useState, useEffect } from "react";
import { StyleSheet, View, Animated, Dimensions } from "react-native";
import AppText from "./AppText";
import AppButton from "./AppButton";
import MultiPicker from "./Swatches";
import useThemeColors from "../hooks/useThemeColors";
import MultiSlider from "@ptomasroos/react-native-multi-slider";
import { useRef } from "react";
import { INITIAL_FILTERS } from "../store/reducers/filters";

const { height: screenHeight, width } = Dimensions.get("window");

const TOP_CONST = screenHeight >= 720 ? 200 : 100;

export default function FilterOptions({
  isVisible,
  priceLimit = 99999,
  updateFilters,
  finalFilters,
}) {
  const colors = useThemeColors();

  const [top] = useState(new Animated.Value(screenHeight + 100));
  const [minPrice, setMinPrice] = useState(finalFilters.minPrice);
  const [maxPrice, setMaxPrice] = useState(priceLimit);
  const [sizes, setSizes] = useState(finalFilters.sizes);
  const [swatchesToggleState, setSwatchesToggleState] = useState(
    INITIAL_FILTERS.sizes.map((_) => 1)
  );
  const sliderUpdateRef = useRef();

  function handleToggle(index) {
    setSwatchesToggleState((swatchState) => [
      ...swatchState.slice(0, index),
      swatchState[index] ? 0 : 1,
      ...swatchState.slice(index + 1),
    ]);
  }

  function handleClear() {
    setMinPrice(INITIAL_FILTERS.minPrice);
    setMaxPrice(priceLimit);
    setSizes(INITIAL_FILTERS.sizes);
    setSwatchesToggleState(
      INITIAL_FILTERS.sizes.map((size) =>
        INITIAL_FILTERS.sizes.includes(size) ? 1 : 0
      )
    );
    updateFilters({
      minPrice: INITIAL_FILTERS.minPrice,
      maxPrice: INITIAL_FILTERS.maxPrice,
      sizes: INITIAL_FILTERS.sizes,
    });
  }

  useEffect(() => {
    if (isVisible) {
      Animated.spring(top, {
        toValue: TOP_CONST,
        useNativeDriver: false,
      }).start();
    } else {
      Animated.spring(top, {
        toValue: screenHeight + 100,
        useNativeDriver: false,
      }).start();
    }
  }, [isVisible]);

  useEffect(() => {
    setSizes(
      swatchesToggleState.reduce(
        (result, curr, index) =>
          curr ? [...result, INITIAL_FILTERS.sizes[index]] : result,
        []
      )
    );
  }, [swatchesToggleState]);

  return (
    <Animated.View
      style={[styles.container, { backgroundColor: colors.light }, { top }]}
    >
      <View style={[styles.wrapper, { color: colors.grey }]}>
        <AppText style={styles.heading}>Filters</AppText>
        <View style={styles.priceFilter}>
          <AppText style={styles.subheading}>Price Filter</AppText>
          <View style={styles.price}>
            <AppText style={[styles.label, , { color: colors.grey }]}>
              {minPrice}
            </AppText>
            <AppText style={[styles.label, { color: colors.grey }]}>
              {maxPrice}
            </AppText>
          </View>
          <MultiSlider
            max={priceLimit}
            step={200}
            values={[minPrice, maxPrice]}
            sliderLength={width * 0.9}
            containerStyle={{ marginLeft: width * 0.05 }}
            markerStyle={{ backgroundColor: colors.secondary }}
            selectedStyle={{ backgroundColor: colors.secondary }}
            onValuesChange={(values) => {
              const [min, max] = values;
              clearTimeout(sliderUpdateRef.current);
              sliderUpdateRef.current = setTimeout(() => {
                setMinPrice(min);
                setMaxPrice(max);
              }, 50);
            }}
          />
        </View>
        <View style={styles.sizeFilter}>
          <AppText style={styles.subheading}>Size Filter</AppText>
          <MultiPicker
            data={INITIAL_FILTERS.sizes}
            toggleState={swatchesToggleState}
            onToggle={(index) => handleToggle(index)}
          />
        </View>
        <View style={styles.buttonWrapper}>
          <AppButton
            title="Clear"
            style={styles.button}
            onPress={handleClear}
          />
          <AppButton
            title="Apply"
            style={styles.button}
            onPress={() => updateFilters({ minPrice, maxPrice, sizes })}
          />
        </View>
      </View>
    </Animated.View>
  );
}

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    width: "100%",
    height: "100%",
    borderRadius: 25,
    top: TOP_CONST,
    elevation: 2,
    overflow: "hidden",
  },
  wrapper: {
    height: screenHeight - TOP_CONST,
    overflow: "hidden",
  },
  heading: {
    fontFamily: "BebasNeue-Bold",
    fontSize: 30,
    marginHorizontal: width * 0.05,
    marginVertical: 20,
  },
  priceFilter: {},
  price: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  subheading: {
    fontFamily: "Montserrat-Bold",
    fontSize: 14.5,
    textTransform: "uppercase",
    marginHorizontal: width * 0.05,
    marginVertical: 25,
    textDecorationLine: "underline",
  },
  label: {
    fontFamily: "Montserrat-Bold",
    fontSize: 14,
    textTransform: "uppercase",
    marginHorizontal: width * 0.05,
    marginVertical: 10,
  },
  labelWrapper: { flexDirection: "row", justifyContent: "space-between" },
  buttonWrapper: {
    flexDirection: "row",
    paddingHorizontal: width * 0.025,
    marginVertical: 30,
  },
  button: {
    flex: 1,
    margin: 5,
    borderRadius: 30,
  },
});
