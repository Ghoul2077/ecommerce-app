import React from "react";
import { Dimensions, FlatList, Animated, StyleSheet, View } from "react-native";
import useThemeColors from "../hooks/useThemeColors";

const { width } = Dimensions.get("window");

export default function Carousel({
  carouselItem: CarouselItem,
  data,
  showIndicators = true,
  indicatorStyle,
}) {
  const ScrollX = new Animated.Value(0);
  let position = Animated.divide(ScrollX, width);
  const colors = useThemeColors();

  return (
    <>
      <AnimatedFlatList
        data={data}
        keyExtractor={(item, index) => String(index)}
        horizontal
        pagingEnabled
        scrollEnabled
        snapToAlignment="center"
        scrollEventThrottle={16}
        decelerationRate="fast"
        showsHorizontalScrollIndicator={false}
        renderItem={({ item }) => <CarouselItem data={item} />}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { x: ScrollX } } }],
          { useNativeDriver: true }
        )}
      />
      {showIndicators && data.length > 1 && (
        <View style={[styles.dotsWrapper, indicatorStyle]}>
          {data.map((_, index) => {
            let opacity = position.interpolate({
              inputRange: [index - 1, index, index + 1],
              outputRange: [0.3, 1, 0.3],
              extrapolate: "clamp",
            });

            return (
              <Animated.View
                key={index}
                style={[
                  styles.dots,
                  { opacity, backgroundColor: colors.primary },
                ]}
              />
            );
          })}
        </View>
      )}
    </>
  );
}

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const styles = StyleSheet.create({
  dotsWrapper: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  dots: {
    height: 8,
    width: 8,
    margin: 7,
    borderRadius: 4,
  },
});
