import React from "react";
import {
  Dimensions,
  Image,
  StyleSheet,
  View,
  TouchableOpacity,
} from "react-native";
import { useSelector } from "react-redux";
import useThemeColors from "../hooks/useThemeColors";
import AppText from "./AppText";

const { width } = Dimensions.get("window");

export default function CatalogueBanner({
  title,
  subtitle,
  source,
  style,
  onPress,
}) {
  const colors = useThemeColors();
  const products = useSelector((state) => state.products.list);

  if (!source) {
    const product = products.find((product) => product.tags.includes(title));
    source = product ? product.images.items[0] : null;
  }

  return (
    <TouchableOpacity
      style={[styles.container, { backgroundColor: colors.light }, style]}
      onPress={onPress}
    >
      <View style={styles.content}>
        <View>
          <AppText style={styles.title}>{title}</AppText>
          <AppText style={[styles.subtitle, { color: colors.grey }]}>
            {subtitle}
          </AppText>
        </View>
      </View>
      <Image
        style={styles.image}
        source={{ uri: source.url }}
        resizeMode="cover"
      />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: width * 0.9,
    marginHorizontal: width * 0.05,
    marginVertical: 10,
    height: 150,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
    borderRadius: 10,
    elevation: 1,
  },
  image: {
    height: "100%",
    width: 150,
  },
  content: { flex: 1, alignItems: "center" },
  title: {
    fontFamily: "BebasNeue-Bold",
    fontSize: 28,
  },
  subtitle: {
    fontFamily: "Montserrat-Bold",
    fontSize: 15,
    marginTop: 5,
    textAlign: "center",
  },
});
