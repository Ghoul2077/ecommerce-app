import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import authReducer from "./reducers/auth";
import cartReducer from "./reducers/cart";
import filtersReducer from "./reducers/filters";
import productsReducer from "./reducers/products";
import firstRunReducer from "./reducers/firstRun";
import themeReducer from "./reducers/theme";
import persistConfig from "./config/persistConfig";

const combinedReducer = combineReducers({
  theme: themeReducer,
  user: authReducer,
  cart: cartReducer,
  filters: filtersReducer,
  products: productsReducer,
  firstRun: firstRunReducer,
});

export default persistReducer(persistConfig, combinedReducer);
