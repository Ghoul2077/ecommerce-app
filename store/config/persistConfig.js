import { default as AsyncStorage } from "@react-native-community/async-storage";

export default persistConfig = {
  key: "root",
  storage: AsyncStorage,
  blacklist: ["filters", "navigation", "products", "cart"],
};
