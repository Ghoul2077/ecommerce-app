import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import { persistStore } from "redux-persist";
import logger from "./middleware/logger";
import persistReducer from "./reducer";

export default function createStore() {
  const store = configureStore({
    reducer: persistReducer,
    middleware: [
      ...getDefaultMiddleware({
        serializableCheck: false,
      }),
      logger({ destination: "console" }),
    ],
  });
  const persistor = persistStore(store);
  return { store, persistor };
}
