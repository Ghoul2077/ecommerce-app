import Toast from "../../components/Toast";

const logger = ({ destination }) => (store) => (next) => (action) => {
  if (destination === "console") {
    console.log(store.getState());
  } else {
    Toast(JSON.stringify(store.getState(), null, 2));
  }

  next(action);
};

export default logger;
