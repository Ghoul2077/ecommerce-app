import { createAction } from "@reduxjs/toolkit";

export const productsApiCallBegan = createAction("api/callBegan");
export const productsApiCallSuccess = createAction("api/callSuccess");
export const productsApiCallFail = createAction("api/callFailed");
