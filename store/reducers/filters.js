import { createSlice } from "@reduxjs/toolkit";

// This value is broadcast to collectionScreen too
export const INITIAL_FILTERS = {
  minPrice: 0,
  maxPrice: 99999,
  sizes: ["XS", "S", "M", "L", "XL", "XXL"],
};

const slice = createSlice({
  name: "filters",
  initialState: INITIAL_FILTERS,
  reducers: {
    updateFilters: (filters, action) => action.payload,
    clearFilters: () => INITIAL_FILTERS,
  },
});

export const { updateFilters, clearFilters } = slice.actions;
export default slice.reducer;
