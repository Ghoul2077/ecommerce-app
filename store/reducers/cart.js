import { createSlice } from "@reduxjs/toolkit";

let lastId = 0;

const slice = createSlice({
  name: "cart",
  initialState: {
    items: [],
    coupon: "",
  },
  reducers: {
    addToCart: (cart, action) => {
      cart.items.push({
        id: lastId++,
        productId: action.payload.productId,
        size: action.payload.choosenSize,
        color: action.payload.choosenColor,
        quantity: action.payload.quantity,
        addTime: Date.now(),
      });
    },
    removeFromCart: (cart, action) => {
      return {
        items: cart.items.filter(
          (cartItem) => cartItem.id !== action.payload.id
        ),
        coupon: cart.coupon,
      };
    },
  },
});

export const { addToCart, removeFromCart } = slice.actions;
export default slice.reducer;
