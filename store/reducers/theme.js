import { createSlice } from "@reduxjs/toolkit";

const slice = createSlice({
  name: "theme",
  initialState: "light",
  reducers: {
    lightSelected: (theme) => {
      theme = "light";
    },
    darkSelected: (theme) => {
      theme = "dark";
    },
  },
});

export const { lightSelected, darkSelected } = slice.actions;
export default slice.reducer;
