import { createSlice } from "@reduxjs/toolkit";

const slice = createSlice({
  name: "user",
  initialState: null,
  reducers: {
    login: (user, action) => {
      user = action.payload.user;
    },
    logout: () => inititialState,
  },
});

export const { login, logout } = slice.actions;
export default slice.reducer;
