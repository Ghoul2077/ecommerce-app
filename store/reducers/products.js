import { createSlice } from "@reduxjs/toolkit";

const slice = createSlice({
  name: "products",
  initialState: {
    list: [],
    loading: false,
    lastFetch: null,
  },
  reducers: {
    productsRequested: (products, action) => {
      products.loading = true;
    },
    productsPopulated: (products, action) => {
      products.list = action.payload.products;
      products.loading = false;
      products.lastFetch = Date.now();
    },
  },
});

export const { productsRequested, productsPopulated } = slice.actions;
export default slice.reducer;
