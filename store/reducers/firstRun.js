import { createSlice } from "@reduxjs/toolkit";

const slice = createSlice({
  name: "firstRun",
  initialState: true,
  reducers: {
    setFistRun: (firstRun) => false,
  },
});

export const { setFistRun } = slice.actions;
export default slice.reducer;
