import React from "react";
import { AppLoading } from "expo";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import createStore from "./store/configureStore";
import useCachedResources from "./hooks/useCachedResources";
import Navigator from "./navigation/MainNavigator";
import { useQuery } from "@apollo/client";
import { ProductsQuery } from "./GraphQL";
import NotFoundScreen from "./screens/NotFoundScreen";
import { productsPopulated } from "./store/reducers/products";

const { store, persistor } = createStore();

export default function Initializer() {
  const { loading: productsFetching, error, data: products } = useQuery(
    ProductsQuery
  );
  const cacheLoadStatus = useCachedResources();
  const isLoadingComplete = !productsFetching && cacheLoadStatus;

  if (!isLoadingComplete) {
    return <AppLoading />;
  } else if (error) {
    return <NotFoundScreen />;
  } else {
    store.dispatch(
      productsPopulated({ products: products.productCollection.items })
    );
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Navigator />
        </PersistGate>
      </Provider>
    );
  }
}
