import { CommonActions, useFocusEffect } from "@react-navigation/native";
import { StatusBar } from "expo-status-bar";
import React from "react";
import { BackHandler, StyleSheet, Text, View } from "react-native";

export default function ProfileScreen({ navigation }) {
  useFocusEffect(() => {
    const backAction = () => {
      navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{ name: "Home" }],
        })
      );
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );

    return () => backHandler.remove();
  }, []);

  return (
    <View style={styles.container}>
      <Text>Profile Screen</Text>
      <StatusBar style="light" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
