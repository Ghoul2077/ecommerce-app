import React, { useEffect } from "react";
import { StatusBar } from "expo-status-bar";
import { useFocusEffect } from "@react-navigation/native";
import {
  StyleSheet,
  BackHandler,
  Alert,
  Dimensions,
  Image,
  ScrollView,
} from "react-native";
import { BannerQuery, CollectionsQuery, HeroQuery } from "../GraphQL";
import Loader from "../components/Loader";
import NotFoundScreen from "./NotFoundScreen";
import { useDispatch, useSelector } from "react-redux";
import useQueries from "../hooks/useQueries";
import Screen from "../components/Screen";
import useThemeColors from "../hooks/useThemeColors";
import Carousel from "../components/Carousel";
import Banner from "../components/Banner";
import FrontPageCarouselItem from "../components/FrontPageCarouselItem";
import ProductShowcase from "../components/ProductShowcase";
import { setFistRun } from "../store/reducers/firstRun";

const { width } = Dimensions.get("window");

export default function HomeScreen({ navigation }) {
  const firstRun = useSelector((state) => state.firstRun);
  const dispatch = useDispatch();
  const { loading, error, queryResults } = useQueries([
    HeroQuery,
    BannerQuery,
    CollectionsQuery,
  ]);
  const products = useSelector((state) => state.products.list);
  const colors = useThemeColors();

  useEffect(() => {
    if (firstRun) {
      dispatch(setFistRun());
    }
  }, [setFistRun, firstRun]);

  useFocusEffect(() => {
    const backAction = () => {
      Alert.alert(
        "Leaving already ?",
        "Are you sure you want to exit the app ?",
        [
          {
            text: "Yes",
            style: "destructive",
            onPress: () => BackHandler.exitApp(),
          },
          { text: "No", style: "cancel", onPress: () => {} },
        ]
      );
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );

    return () => backHandler.remove();
  }, []);

  const handleCardTap = (id) => {
    navigation.push("Single Product", { productId: id });
  };

  if (loading) {
    return <Loader />;
  } else if (error) {
    return <NotFoundScreen />;
  } else {
    const [heroData, bannerData, collectionsData] = queryResults;

    return (
      <Screen
        headerShown={true}
        style={[styles.container, { backgroundColor: colors.white }]}
      >
        <Image
          style={styles.background}
          source={require("../assets/patterns/bg_pattern2.png")}
          resizeMode="cover"
        />
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={styles.scrollWrapper}
        >
          <Carousel
            carouselItem={FrontPageCarouselItem}
            data={heroData.hero.images.items}
            indicatorStyle={styles.dotsStyle}
          />
          {bannerData.bannerCollection.items.map(
            ({ associatedTag, image }, index) => (
              <Banner
                source={image.url}
                key={index}
                onPress={() =>
                  navigation.navigate("Collection", { tag: associatedTag })
                }
              />
            )
          )}
          {collectionsData.collections.items.map((collection, index) => {
            const filteredProducts = products.filter((product) =>
              collection.productsCollection.items.some(
                (p) => p.sys.id === product.sys.id
              )
            );

            return (
              <ProductShowcase
                key={index}
                heading={collection.name}
                data={filteredProducts}
                scrollViewStyle={styles.showcase}
                onPress={handleCardTap}
                cardStyle={styles.card}
              />
            );
          })}
        </ScrollView>
        <StatusBar style="light" />
      </Screen>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 0,
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
    opacity: 0.05,
  },
  scrollWrapper: {
    width: "100%",
    paddingVertical: 10,
  },
  dotsStyle: {
    marginVertical: 5,
  },
  showcase: {
    marginBottom: 10,
  },
  card: {
    marginLeft: width * 0.04,
  },
});
