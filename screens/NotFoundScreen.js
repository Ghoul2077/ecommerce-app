import React from "react";
import { Image, StyleSheet } from "react-native";
import AppButton from "../components/AppButton";
import AppText from "../components/AppText";
import Screen from "../components/Screen";

export default function NotFoundScreen({ navigation }) {
  const colors = useThemeColors();

  return (
    <Screen style={[styles.container, { backgroundColor: colors.secondary }]}>
      <Image
        style={styles.image}
        source={require("../assets/images/404.png")}
        resizeMode="contain"
      />
      <AppText style={styles.heading}>OOPS !</AppText>
      <AppText style={[styles.subHeading, { color: colors.text }]}>
        The page are looking for could not be found :(
      </AppText>
      <AppButton
        style={styles.backBtn}
        title="Back To Home"
        onPress={() => navigation.replace("Home")}
      />
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
  },
  image: { width: 400, height: 300, margin: 10 },
  heading: {
    fontSize: 40,
    fontWeight: "bold",
    margin: 20,
  },
  subHeading: {
    fontFamily: "Montserrat-Medium",
    width: 250,
    fontSize: 15,
    textAlign: "center",
    margin: 20,
  },
  backBtn: {
    backgroundColor: "black",
    width: 300,
    padding: 15,
  },
});
