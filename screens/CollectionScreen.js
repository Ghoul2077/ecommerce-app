import React, { useState, useEffect } from "react";
import { StatusBar } from "expo-status-bar";
import {
  Image,
  StyleSheet,
  View,
  Dimensions,
  ScrollView,
  Animated,
  BackHandler,
} from "react-native";
import Constants from "expo-constants";
import { useSelector } from "react-redux";
import { useFocusEffect } from "@react-navigation/native";
import AppButton from "../components/AppButton";
import AppText from "../components/AppText";
import Screen from "../components/Screen";
import ProductCard from "../components/ProductCard";
import FilterOptions from "../components/FilterOptions";
import useThemeColors from "../hooks/useThemeColors";
import { INITIAL_FILTERS } from "../store/reducers/filters";

const { width } = Dimensions.get("window");

export default function CollectionScreen({ route, navigation }) {
  const products = useSelector((state) => state.products.list);
  const colors = useThemeColors();

  const [filters, setFilters] = useState(INITIAL_FILTERS);

  const [isFilterVisible, setIsFilterVisible] = useState(false);
  const [scale] = useState(new Animated.Value(1));
  const { minPrice, maxPrice, sizes } = filters;
  const { tag } = route.params;
  let { queriedProducts } = route.params;

  if (!queriedProducts) {
    queriedProducts = products.filter((product) => product.tags.includes(tag));
  }

  const filteredProducts = queriedProducts.filter(
    (product) =>
      product.price >= minPrice &&
      product.price <= maxPrice &&
      sizes.some((size) => product.sizes.includes(size))
  );

  const MAX_PRICE = queriedProducts.reduce(
    (max, curr) => (max > curr.price ? max : curr.price),
    0
  );

  function handlePress(id) {
    setIsFilterVisible(false);
    navigation.navigate("Single Product", { productId: id });
  }

  useFocusEffect(() => {
    const backAction = () => {
      if (isFilterVisible) {
        setIsFilterVisible(false);
        return true;
      }
      return false;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );

    return () => backHandler.remove();
  }, [isFilterVisible]);

  useEffect(() => {
    if (isFilterVisible) {
      Animated.spring(scale, {
        toValue: 0.9,
        useNativeDriver: true,
      }).start();
    } else {
      Animated.spring(scale, {
        toValue: 1,
        useNativeDriver: true,
      }).start();
    }
  }, [isFilterVisible]);

  return (
    <Screen headerShown={true} style={styles.container}>
      <Animated.View
        style={[
          styles.wrapper,
          { transform: [{ scale }] },
          isFilterVisible && { borderRadius: 15 },
          { backgroundColor: colors.white },
        ]}
      >
        <Image
          style={styles.background}
          source={require("../assets/patterns/bg_pattern2.png")}
          resizeMode="cover"
        />
        <View style={styles.options}>
          <AppButton
            style={styles.backBtn}
            leftIconName="ios-arrow-back"
            iconSize={25}
            iconColor="black"
            onPress={() => navigation.goBack()}
          />
          <AppButton
            style={styles.backBtn}
            leftIconName="ios-options"
            iconSize={25}
            iconColor="black"
            onPress={() =>
              setIsFilterVisible((isFilterVisible) => !isFilterVisible)
            }
          />
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          <AppText style={styles.heading}>{tag}</AppText>
          <View style={styles.productsContainer}>
            {filteredProducts.map((product, index) => {
              return (
                <ProductCard
                  key={index}
                  title={product.name}
                  subtitle={"₹ ".concat(product.price)}
                  alternateSubtitle={product.discountedPrice}
                  source={product.images.items[0]}
                  style={styles.card}
                  onPress={() => handlePress(product.sys.id)}
                />
              );
            })}
          </View>
        </ScrollView>
      </Animated.View>
      <FilterOptions
        isVisible={isFilterVisible}
        priceLimit={MAX_PRICE ? MAX_PRICE : undefined}
        updateFilters={setFilters}
        finalFilters={filters}
      />
      <StatusBar style="dark" />
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "black",
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
    opacity: 0.05,
  },
  heading: {
    fontFamily: "BebasNeue-Bold",
    fontSize: 45,
    marginHorizontal: width * 0.05,
    marginVertical: 10,
  },
  options: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: width * 0.015,
    paddingVertical: 2,
  },
  backBtn: {
    width: 50,
    height: 50,
    backgroundColor: "transparent",
    zIndex: 100,
  },
  productsContainer: {
    flexDirection: "row",
    width: "100%",
    flexWrap: "wrap",
    justifyContent: "space-between",
    paddingHorizontal: width * 0.025,
  },
  card: {
    margin: 10,
    width: width * 0.4,
    height: 300,
  },
  wrapper: {
    flex: 1,
    paddingTop: Constants.statusBarHeight,
  },
});
