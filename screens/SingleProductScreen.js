import React, { useEffect, useState } from "react";
import { StyleSheet, View, Dimensions } from "react-native";
import { StatusBar } from "expo-status-bar";
import Constants from "expo-constants";
import { useDispatch, useSelector } from "react-redux";
import Carousel from "../components/Carousel";
import SingleProductCarouselItem from "../components/SingleProductCarouselItem";
import AppText from "../components/AppText";
import AppButton from "../components/AppButton";
import AppPicker from "../components/AppPicker";
import Toast from "../components/Toast";
import AppToast from "../components/AppToast";
import useThemeColors from "../hooks/useThemeColors";
import { addToCart } from "../store/reducers/cart";

const { width, height } = Dimensions.get("window");

export default function SingleProductScreen({ navigation, route }) {
  const colors = useThemeColors();
  const dispatch = useDispatch();
  const products = useSelector((state) => state.products.list);

  const { productId } = route.params;
  const {
    sys,
    name,
    price,
    images,
    description,
    colors: productColors,
    sizes,
    discountedPrice,
  } = products.filter((product) => product.sys.id === productId)[0];

  const [chosenColor, setChosenColor] = useState(null);
  const [chosenSize, setChosenSize] = useState(null);
  const [shown, setShown] = useState(false);

  function handleChangeColor(color) {
    setChosenColor(color);
  }

  function handleChangeSize(size) {
    setChosenSize(size);
  }

  function pushToCart() {
    if (chosenColor && chosenSize) {
      dispatch(
        addToCart({
          productId: sys.id,
          choosenSize: chosenSize,
          choosenColor: chosenColor,
          quantity: 1,
        })
      );
      setShown(true);
    } else Toast("Choose missing variation options");
  }

  useEffect(() => {
    const timer = setTimeout(() => {
      setShown(false);
    }, 5000);
    return () => clearTimeout(timer);
  }, [shown]);

  return (
    <View style={[styles.container, { backgroundColor: colors.light }]}>
      <AppButton
        style={styles.backBtn}
        leftIconName="ios-arrow-back"
        iconSize={25}
        iconColor="black"
        onPress={() => navigation.goBack()}
      />
      <Carousel
        carouselItem={SingleProductCarouselItem}
        data={images.items}
        indicatorStyle={styles.dotsStyle}
      />
      <View style={styles.contentWrapper}>
        <View style={styles.productInfo}>
          <AppText style={styles.productName}>{name}</AppText>
          <View style={{ flexDirection: "row" }}>
            <AppText
              style={[
                styles.productPrice,
                discountedPrice && {
                  textDecorationLine: "line-through",
                  marginRight: 5,
                },
              ]}
            >
              {"₹ ".concat(price)}
            </AppText>
            {discountedPrice && (
              <AppText style={[styles.productPrice, { color: colors.error }]}>
                {"₹ ".concat(discountedPrice)}
              </AppText>
            )}
          </View>
        </View>
        <View style={styles.variationsWrapper}>
          <AppPicker
            title="COLOR"
            style={[
              styles.picker,
              { marginRight: 5 },
              height > 720 && { padding: 15 },
            ]}
            data={productColors}
            selection={chosenColor}
            changeSelection={handleChangeColor}
          />
          <AppPicker
            title="SIZE"
            style={[
              styles.picker,
              { marginLeft: 5 },
              height > 720 && { padding: 15 },
            ]}
            data={sizes}
            selection={chosenSize}
            changeSelection={handleChangeSize}
          />
        </View>
        <AppButton
          style={[styles.addToCart, height > 720 && { padding: 17 }]}
          title="Add to Cart"
          onPress={pushToCart}
        />
      </View>
      {shown && (
        <AppToast
          text="Added to Cart"
          buttonText="View"
          style={{ opacity: 0.95 }}
          onPress={() => navigation.navigate("Cart")}
        />
      )}
      <StatusBar styles="light" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  backBtn: {
    position: "absolute",
    top: Constants.statusBarHeight + 5,
    left: 5,
    width: 50,
    height: 50,
    backgroundColor: "transparent",
    zIndex: 100,
  },
  dotsStyle: {
    position: "absolute",
    right: 15,
    top: Constants.statusBarHeight + 20,
  },
  contentWrapper: {
    height: height * 0.3,
    justifyContent: "space-around",
    alignItems: "center",
    maxHeight: 400,
  },
  productInfo: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: width * 0.85,
  },
  productName: {
    fontFamily: "BebasNeue-Bold",
    fontSize: 28,
    width: width * 0.5,
  },
  productPrice: {
    fontFamily: "Montserrat-Bold",
    fontSize: 16,
  },
  variationsWrapper: {
    flexDirection: "row",
    width: width * 0.85,
    justifyContent: "space-between",
  },
  picker: {
    flex: 1,
    borderRadius: 25,
    paddingVertical: 10,
  },
  addToCart: {
    width: width * 0.85,
    marginHorizontal: 20,
    borderRadius: 25,
    padding: 13,
  },
});
