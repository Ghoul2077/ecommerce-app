import { useQuery } from "@apollo/client";
import { CommonActions, useFocusEffect } from "@react-navigation/native";
import { StatusBar } from "expo-status-bar";
import React from "react";
import {
  BackHandler,
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
} from "react-native";
import AppText from "../components/AppText";
import CatalogueBanner from "../components/CatalogueBanner";
import Loader from "../components/Loader";
import Screen from "../components/Screen";
import { CatalogueQuery } from "../GraphQL";
import NotFoundScreen from "./NotFoundScreen";

const { width } = Dimensions.get("window");

export default function CatalogueScreen({ navigation }) {
  const { loading, error, data } = useQuery(CatalogueQuery);

  useFocusEffect(() => {
    const backAction = () => {
      navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{ name: "Home" }],
        })
      );
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );

    return () => backHandler.remove();
  }, []);

  if (loading) {
    return <Loader />;
  } else if (error) {
    return <NotFoundScreen />;
  } else {
    const catalogue = data.catalogueCollection.items;

    return (
      <Screen headerShown={true}>
        <Image
          style={styles.background}
          source={require("../assets/patterns/bg_pattern2.png")}
          resizeMode="cover"
        />
        <ScrollView
          style={styles.container}
          contentContainerStyle={{ flexGrow: 1 }}
          showsVerticalScrollIndicator={false}
        >
          <AppText style={styles.heading}>Catalogue</AppText>
          {catalogue.map((category, index) => (
            <CatalogueBanner
              key={index}
              title={category.name}
              // subtitle={tag.productCount}
              source={category.image}
              onPress={() =>
                navigation.navigate("Collection", { tag: category.name })
              }
            />
          ))}
        </ScrollView>
        <StatusBar style="light" />
      </Screen>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexGrow: 1,
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
    opacity: 0.05,
  },
  heading: {
    fontFamily: "BebasNeue-Bold",
    fontSize: 39,
    marginHorizontal: width * 0.05,
    marginVertical: 25,
  },
});
