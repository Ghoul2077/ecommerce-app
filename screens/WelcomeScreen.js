import React from "react";
import { StyleSheet, Image, View, Dimensions } from "react-native";
import { StatusBar } from "expo-status-bar";
import AppButton from "../components/AppButton";
import AppText from "../components/AppText";
import Constants from "expo-constants";
import Screen from "../components/Screen";
import useThemeColors from "../hooks/useThemeColors";

const { height } = Dimensions.get("window");

export default function WelcomeScreen({ navigation }) {
  const colors = useThemeColors();

  return (
    <Screen style={[styles.container, { backgroundColor: colors.background }]}>
      <Image
        style={styles.bgImage}
        source={require("../assets/patterns/bg_pattern1.png")}
        resizeMode="cover"
      />
      <AppButton
        title="Skip"
        style={styles.skipBtn}
        textStyle={[styles.skipBtnText, { color: colors.text }]}
        onPress={() => navigation.replace("HomeStack")}
      />
      <View style={styles.content}>
        <Image
          style={styles.logo}
          source={require("../assets/images/logo.png")}
        />
        <AppText style={[styles.text, { color: colors.primary }]}>
          Create
        </AppText>
        <AppText style={styles.moto}>Self Improvement</AppText>
        <AppText style={styles.catchphrase}>
          Motivation And Your Personal Vision An Unbeatable Force
        </AppText>
      </View>
      <View style={styles.buttonContainer}>
        <AppButton
          title="Login"
          style={[styles.btn, { backgroundColor: colors.secondary }]}
          textStyle={styles.btnText}
          onPress={() => navigation.navigate("Login")}
        />
        <AppButton
          title="Sign Up"
          style={styles.btn}
          textStyle={styles.btnText}
          onPress={() => navigation.navigate("SignUp")}
        />
      </View>
      <StatusBar style="auto" />
    </Screen>
  );
}

const styles = StyleSheet.create({
  bgImage: {
    position: "absolute",
    width: "100%",
    height: "100%",
    opacity: 0.05,
  },
  btn: {
    marginBottom: height < 600 ? 10 : 20,
  },
  btnText: {
    fontFamily: "Montserrat-Bold",
  },
  buttonContainer: {
    position: "absolute",
    bottom: 0,
    width: "100%",
    paddingHorizontal: 20,
  },
  catchphrase: {
    fontFamily: "Montserrat-Medium",
    textAlign: "center",
    fontSize: 15,
    width: 250,
    marginTop: 20,
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  content: {
    justifyContent: "center",
    alignItems: "center",
  },
  logo: {
    height: 150,
    width: 150,
  },
  moto: {
    fontFamily: "BebasNeue-Bold",
    fontSize: 35,
    marginTop: 10,
  },
  skipBtn: {
    position: "absolute",
    top: Constants.statusBarHeight,
    right: 10,
    backgroundColor: "transparent",
  },
  skipBtnText: {
    textTransform: "uppercase",
  },
  text: {
    fontFamily: "DancingScript-Bold",
    fontSize: 35,
    marginTop: 15,
  },
});
