import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import {
  CatalogueStack,
  DealsStack,
  HomeStack,
  ProfileStack,
  WelcomeStack,
} from "./StackNavigators";
import NotFoundScreen from "../screens/NotFoundScreen";
import { useSelector } from "react-redux";

const MainStackNavigator = createStackNavigator();

export default function Navigator() {
  const firstRun = useSelector((state) => state.firstRun);

  return (
    <NavigationContainer>
      <MainStackNavigator.Navigator screenOptions={{ headerShown: false }}>
        {firstRun && (
          <MainStackNavigator.Screen
            name="WelcomeStack"
            component={WelcomeStack}
          />
        )}
        <MainStackNavigator.Screen name="HomeStack" component={HomeStack} />
        <MainStackNavigator.Screen
          name="CatalogueStack"
          component={CatalogueStack}
        />
        <MainStackNavigator.Screen name="DealsStack" component={DealsStack} />
        <MainStackNavigator.Screen
          name="ProfileStack"
          component={ProfileStack}
        />
        <MainStackNavigator.Screen name="NotFound" component={NotFoundScreen} />
      </MainStackNavigator.Navigator>
    </NavigationContainer>
  );
}
