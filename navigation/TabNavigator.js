import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import CatalogueScreen from "../screens/CatalogueScreen";
import DealsScreen from "../screens/DealsScreen";
import HomeScreen from "../screens/HomeScreen";
import ProfileScreen from "../screens/ProfileScreen";
import { Ionicons } from "@expo/vector-icons";

const Tabs = createBottomTabNavigator();

const setIcon = (route) => {
  return ({ color, size }) => {
    let iconName;

    switch (route.name) {
      case "Home":
        iconName = "ios-compass";
        break;
      case "Catalogue":
        iconName = "ios-pricetags";
        break;
      case "Deals":
        iconName = "ios-flash";
        break;
      case "Profile":
        iconName = "ios-person";
        break;
      default:
        iconName = "ios-help";
    }

    return <Ionicons name={iconName} size={size} color={color} />;
  };
};

const screenOptions = ({ route }) => ({
  tabBarIcon: setIcon(route),
});

const tabBarOptions = {
  activeTintColor: "#F7B579",
  inactiveTintColor: "black",
  showLabel: false,
};

export function TabStack() {
  return (
    <Tabs.Navigator screenOptions={screenOptions} tabBarOptions={tabBarOptions}>
      <Tabs.Screen name="Home" component={HomeScreen} />
      <Tabs.Screen name="Catalogue" component={CatalogueScreen} />
      <Tabs.Screen name="Deals" component={DealsScreen} />
      <Tabs.Screen name="Profile" component={ProfileScreen} />
    </Tabs.Navigator>
  );
}
