import React from "react";
import { Dimensions } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { TabStack } from "./TabNavigator";
import CollectionScreen from "../screens/CollectionScreen";
import SingleProductScreen from "../screens/SingleProductScreen";
import CartScreen from "../screens/CartScreen";
import LoginScreen from "../screens/LoginScreen";
import SignupScreen from "../screens/SignupScreen";
import AppButton from "../components/AppButton";
import Searchbar from "../components/Searchbar";
import colors from "../constants/Colors";
import WelcomeScreen from "../screens/WelcomeScreen";

const { height } = Dimensions.get("window");

const tabScreenOptions = (navigation) => {
  return {
    headerShown: true,
    headerStyle: {
      backgroundColor: colors.light.primary,
      elevation: 2,
      height: height > 800 ? 100 : 85,
    },
    headerTitle: () => <Searchbar />,
    headerRight: () => (
      <AppButton
        leftIconName="ios-cart"
        style={{ paddingRight: 15 }}
        onPress={() => navigation.navigate("Cart")}
      />
    ),
  };
};

const generalScreenOptions = (navigation) => {
  return { headerShown: false };
};

const cartScreenOptions = (navigation) => {
  return {
    headerShown: true,
    headerStyle: {
      backgroundColor: colors.light.background,
      elevation: 0,
      height: height > 800 ? 100 : 85,
    },
    headerTitle: "Cart",
    headerRight: null,
    headerTitleAlign: "center",
    headerLeft: () => (
      <AppButton
        style={{
          width: 50,
          height: 50,
          backgroundColor: "transparent",
          marginHorizontal: 5,
        }}
        leftIconName="ios-arrow-back"
        iconSize={25}
        iconColor="black"
        onPress={() => navigation.goBack()}
      />
    ),
    headerTitleStyle: {
      fontFamily: "Montserrat-Bold",
      textTransform: "uppercase",
      fontSize: 15,
    },
  };
};

const HomeStackNavigator = createStackNavigator();

export function HomeStack() {
  return (
    <HomeStackNavigator.Navigator screenOptions={generalScreenOptions}>
      <HomeStackNavigator.Screen
        name="Tabs"
        component={TabStack}
        options={({ navigation }) => tabScreenOptions(navigation)}
      />
      <HomeStackNavigator.Screen
        name="Collection"
        component={CollectionScreen}
      />
      <HomeStackNavigator.Screen
        name="Single Product"
        component={SingleProductScreen}
      />
      <HomeStackNavigator.Screen
        name="Cart"
        component={CartScreen}
        options={({ navigation }) => cartScreenOptions(navigation)}
      />
    </HomeStackNavigator.Navigator>
  );
}

const CatalogueStackNavigator = createStackNavigator();

export function CatalogueStack() {
  return (
    <CatalogueStackNavigator.Navigator screenOptions={generalScreenOptions}>
      <CatalogueStackNavigator.Screen
        name="Tabs"
        component={TabStack}
        options={({ navigation }) => tabScreenOptions(navigation)}
      />
      <CatalogueStackNavigator.Screen
        name="Single Product"
        component={SingleProductScreen}
      />
      <CatalogueStackNavigator.Screen
        name="Cart"
        component={CartScreen}
        options={({ navigation }) => cartScreenOptions(navigation)}
      />
    </CatalogueStackNavigator.Navigator>
  );
}

const DealsStackNavigator = createStackNavigator();

export function DealsStack() {
  return (
    <DealsStackNavigator.Navigator screenOptions={generalScreenOptions}>
      <DealsStackNavigator.Screen
        name="Tabs"
        component={TabStack}
        options={({ navigation }) => tabScreenOptions(navigation)}
      />
      <DealsStackNavigator.Screen
        name="Single Product"
        component={SingleProductScreen}
      />
      <DealsStackNavigator.Screen
        name="Cart"
        component={CartScreen}
        options={({ navigation }) => cartScreenOptions(navigation)}
      />
    </DealsStackNavigator.Navigator>
  );
}

const ProfileStackNavigator = createStackNavigator();

export function ProfileStack() {
  return (
    <ProfileStackNavigator.Navigator screenOptions={generalScreenOptions}>
      <ProfileStackNavigator.Screen
        name="Tabs"
        component={TabStack}
        options={({ navigation }) => tabScreenOptions(navigation)}
      />
      <ProfileStackNavigator.Screen name="Login" component={LoginScreen} />
      <ProfileStackNavigator.Screen name="SignUp" component={SignupScreen} />
    </ProfileStackNavigator.Navigator>
  );
}

const WelcomeStackNavigator = createStackNavigator();

export function WelcomeStack() {
  return (
    <WelcomeStackNavigator.Navigator screenOptions={generalScreenOptions}>
      <WelcomeStackNavigator.Screen name="Welcome" component={WelcomeScreen} />
      <WelcomeStackNavigator.Screen name="Login" component={LoginScreen} />
      <WelcomeStackNavigator.Screen name="SignUp" component={SignupScreen} />
    </WelcomeStackNavigator.Navigator>
  );
}
