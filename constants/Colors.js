const tintColorLight = "#2f95dc";
const tintColorDark = "#fff";

const common = {
  primary: "#6478D3",
  secondary: "#F7B579",
};

export default {
  light: {
    ...common,
    secondaryDark: "#ED872A",
    text: "#22242A",
    background: "#F1F8FC",
    error: "#FF697C",
    success: "#53B87F",
    white: "#FFFFFF",
    light: "#F7F8FA",
    medium: "#CDCDD7",
    grey: "#8D92A3",
    tint: tintColorLight,
    tabIconDefault: "#ccc",
    tabIconSelected: tintColorLight,
  },
  dark: {
    ...common,
    text: "#fff",
    background: "#000",
    tint: tintColorDark,
    tabIconDefault: "#ccc",
    tabIconSelected: tintColorDark,
  },
};
