import React from "react";
import { ApolloClient, ApolloProvider, InMemoryCache } from "@apollo/client";
import Initializer from "./Initializer";
import {
  SPACE_ID,
  CONTENT_DELIVERY_API_KEY,
  SENTRY_DSN,
} from "@env";
import * as Sentry from "sentry-expo";

Sentry.init({
  dsn: `${SENTRY_DSN}`,
  enableInExpoDevelopment: true,
  debug: true,
});

const client = new ApolloClient({
  uri: `https://graphql.contentful.com/content/v1/spaces/${SPACE_ID}`,
  credentials: "same-origin",
  headers: {
    Authorization: `Bearer ${CONTENT_DELIVERY_API_KEY}`,
  },
  cache: new InMemoryCache(),
});

export default function App() {
  return (
    <ApolloProvider client={client}>
      <Initializer />
    </ApolloProvider>
  );
}
