# <div align="center"> ![app icon](./.gitlab/readme-images/logo.png)<div>**Ecommerce App**</div></div>

<div align="center">A react native based ecommerce app</div>
<br />

![screenshots of app](./.gitlab/readme-images/screens.png)

## Showcase

<br />
<img src="./.gitlab/readme-video/output.gif" width="275" height="600" />
<br />
<br />

## Newest Release

First release will be with version 1.0

## Features

Features of Ecommerce App include:

- Handling of products from backend service with live updates to user
- Login & Logout functionality
- Ability to create deals and discounts
- Dynamic search
- Checkout (WIP)
- Automatic light and dark themes (Ability is there but not yet implemented)
- Ability to show Adverts and Banners
- On the air update capability

## Services / Stack

- React Native (Expo)
- GraphQL (Querying)
- Contentful (Headless CMS)
- Firebase (Authentication and Storage)
- Stripe / Razor pay (Payment and Checkout)

## License

    Copyright 2015 Javier Tomás

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

## Disclaimer

The developer of this application does not have any affiliation with the content providers available.
