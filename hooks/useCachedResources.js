import { Ionicons } from '@expo/vector-icons';
import * as Font from 'expo-font';
import * as SplashScreen from 'expo-splash-screen';
import { useState, useEffect } from 'react';

export default function useCachedResources() {
  const [isLoadingComplete, setLoadingComplete] = useState(false);

  // Load any resources or data that we need prior to rendering the app
  useEffect(() => {
    async function loadResourcesAndDataAsync() {
      try {
        SplashScreen.preventAutoHideAsync();

        // Load fonts
        await Font.loadAsync({
          ...Ionicons.font,
          'BebasNeue': require("../assets/fonts/BebasNeue-Regular.ttf"),
          'BebasNeue-Thin': require("../assets/fonts/BebasNeue-Thin.ttf"),
          'BebasNeue-Bold': require("../assets/fonts/BebasNeue-Bold.ttf"),
          'Montserrat': require("../assets/fonts/Montserrat-Regular.ttf"),
          'Montserrat-Medium': require("../assets/fonts/Montserrat-Medium.ttf"),
          'Montserrat-Bold': require("../assets/fonts/Montserrat-Bold.ttf"),
          'DancingScript': require("../assets/fonts/DancingScript-Regular.ttf"),
          'DancingScript-Bold': require("../assets/fonts/DancingScript-Bold.ttf"),
          'DancingScript-Medium': require("../assets/fonts/DancingScript-Medium.ttf"),
        });
      } catch (e) {
        // We might want to provide this error information to an error reporting service
        console.warn(e);
      } finally {
        setLoadingComplete(true);
        SplashScreen.hideAsync();
      }
    }

    loadResourcesAndDataAsync();
  }, []);

  return isLoadingComplete;
}
