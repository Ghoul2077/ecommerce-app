import React from "react";
import { useQuery } from "@apollo/client";

export default function useQueries(queries) {
  const results = queries.map((query) => useQuery(query));
  const loading = results.some((result) => result.loading);
  const error = results.some((result) => result.error);
  const queryResults = results.map((result) => result.data);
  return { loading, error, queryResults };
}
