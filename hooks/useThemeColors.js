import React from "react";
import { useSelector } from "react-redux";
import colors from "../constants/Colors";

export default function useThemeColors() {
  const theme = useSelector((state) => state.theme);
  return colors[theme];
}
