import { gql } from "@apollo/client";

const ProductsQuery = gql`
  {
    productCollection(order: name_ASC) {
      items {
        sys {
          id
        }
        name
        price
        discountedPrice
        images: imagesCollection {
          items {
            url
          }
        }
        description
        tags
        sizes
        colors
      }
    }
  }
`;

const HeroQuery = gql`
  {
    hero(id: "7iqMOEKLzCMpd5ZztCw2hk") {
      images: imagesCollection {
        items {
          url
        }
      }
    }
  }
`;

const CatalogueQuery = gql`
  {
    catalogueCollection(order: name_ASC) {
      items {
        name
        image {
          url
        }
      }
    }
  }
`;

const CollectionsQuery = gql`
  {
    collections: collectionCollection {
      items {
        name
        productsCollection {
          items {
            sys {
              id
            }
          }
        }
      }
    }
  }
`;

const BannerQuery = gql`
  {
    bannerCollection {
      items {
        associatedTag
        image {
          url
        }
      }
    }
  }
`;

const DealsQuery = gql`
  {
    dealsCollection(order: priority_DESC) {
      items {
        dealName
        description
        productsCollection {
          items {
            sys {
              id
            }
          }
        }
        endTime
      }
    }
  }
`;

export {
  ProductsQuery,
  HeroQuery,
  CatalogueQuery,
  CollectionsQuery,
  BannerQuery,
  DealsQuery,
};
